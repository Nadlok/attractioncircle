/********************************************************************************
** Form generated from reading UI file 'attractioncircles.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ATTRACTIONCIRCLES_H
#define UI_ATTRACTIONCIRCLES_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_attractionCircles
{
public:
    QGridLayout *gridLayout;
    QGraphicsView *graphicsView;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_5;
    QLabel *lblOpacity;
    QLabel *label;
    QSlider *slOpacity;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_3;
    QLabel *lblNumCircles;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnDeleteAll;

    void setupUi(QWidget *attractionCircles)
    {
        if (attractionCircles->objectName().isEmpty())
            attractionCircles->setObjectName(QStringLiteral("attractionCircles"));
        attractionCircles->resize(800, 600);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(attractionCircles->sizePolicy().hasHeightForWidth());
        attractionCircles->setSizePolicy(sizePolicy);
        attractionCircles->setMinimumSize(QSize(800, 600));
        attractionCircles->setMaximumSize(QSize(800, 600));
        QIcon icon;
        icon.addFile(QStringLiteral("ico/ac.ico"), QSize(), QIcon::Normal, QIcon::Off);
        attractionCircles->setWindowIcon(icon);
        gridLayout = new QGridLayout(attractionCircles);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        graphicsView = new QGraphicsView(attractionCircles);
        graphicsView->setObjectName(QStringLiteral("graphicsView"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(graphicsView->sizePolicy().hasHeightForWidth());
        graphicsView->setSizePolicy(sizePolicy1);
        graphicsView->setMinimumSize(QSize(256, 256));
        graphicsView->setMouseTracking(false);
        graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        graphicsView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustIgnored);
        graphicsView->setRenderHints(QPainter::Antialiasing|QPainter::TextAntialiasing);
        graphicsView->setDragMode(QGraphicsView::RubberBandDrag);

        gridLayout->addWidget(graphicsView, 0, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_5);

        lblOpacity = new QLabel(attractionCircles);
        lblOpacity->setObjectName(QStringLiteral("lblOpacity"));
        lblOpacity->setMaximumSize(QSize(1666667, 20));

        horizontalLayout->addWidget(lblOpacity);

        label = new QLabel(attractionCircles);
        label->setObjectName(QStringLiteral("label"));
        label->setMinimumSize(QSize(10, 0));

        horizontalLayout->addWidget(label);

        slOpacity = new QSlider(attractionCircles);
        slOpacity->setObjectName(QStringLiteral("slOpacity"));
        slOpacity->setMinimum(1);
        slOpacity->setMaximum(100);
        slOpacity->setValue(50);
        slOpacity->setOrientation(Qt::Horizontal);

        horizontalLayout->addWidget(slOpacity);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);

        label_3 = new QLabel(attractionCircles);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMaximumSize(QSize(16777215, 20));

        horizontalLayout_4->addWidget(label_3);

        lblNumCircles = new QLabel(attractionCircles);
        lblNumCircles->setObjectName(QStringLiteral("lblNumCircles"));
        lblNumCircles->setMinimumSize(QSize(10, 0));
        lblNumCircles->setMaximumSize(QSize(16777215, 20));

        horizontalLayout_4->addWidget(lblNumCircles);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        btnDeleteAll = new QPushButton(attractionCircles);
        btnDeleteAll->setObjectName(QStringLiteral("btnDeleteAll"));
        btnDeleteAll->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_2->addWidget(btnDeleteAll);


        verticalLayout->addLayout(horizontalLayout_2);


        horizontalLayout_3->addLayout(verticalLayout);


        gridLayout->addLayout(horizontalLayout_3, 1, 0, 1, 1);

        graphicsView->raise();

        retranslateUi(attractionCircles);
        QObject::connect(slOpacity, SIGNAL(valueChanged(int)), label, SLOT(setNum(int)));

        QMetaObject::connectSlotsByName(attractionCircles);
    } // setupUi

    void retranslateUi(QWidget *attractionCircles)
    {
        attractionCircles->setWindowTitle(QApplication::translate("attractionCircles", "Attraction circles", Q_NULLPTR));
        lblOpacity->setText(QApplication::translate("attractionCircles", "Opacity circles", Q_NULLPTR));
        label->setText(QApplication::translate("attractionCircles", "00", Q_NULLPTR));
        label_3->setText(QApplication::translate("attractionCircles", "Number of circles", Q_NULLPTR));
        lblNumCircles->setText(QApplication::translate("attractionCircles", "0", Q_NULLPTR));
        btnDeleteAll->setText(QApplication::translate("attractionCircles", "&Delete all circles", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class attractionCircles: public Ui_attractionCircles {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ATTRACTIONCIRCLES_H
