/****************************************************************************
** Meta object code from reading C++ file 'findcollide.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../attractionCircles/findcollide.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'findcollide.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_findCollide_t {
    QByteArrayData data[7];
    char stringdata0[117];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_findCollide_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_findCollide_t qt_meta_stringdata_findCollide = {
    {
QT_MOC_LITERAL(0, 0, 11), // "findCollide"
QT_MOC_LITERAL(1, 12, 23), // "signalFindCollideFinish"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 19), // "signalStartPathCalc"
QT_MOC_LITERAL(4, 57, 21), // "QList<QGraphicsItem*>"
QT_MOC_LITERAL(5, 79, 22), // "slotFindCollideInScene"
QT_MOC_LITERAL(6, 102, 14) // "slotPathIsCalc"

    },
    "findCollide\0signalFindCollideFinish\0"
    "\0signalStartPathCalc\0QList<QGraphicsItem*>\0"
    "slotFindCollideInScene\0slotPathIsCalc"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_findCollide[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,
       3,    1,   35,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   38,    2, 0x0a /* Public */,
       6,    0,   41,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    2,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 4,    2,
    QMetaType::Void,

       0        // eod
};

void findCollide::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        findCollide *_t = static_cast<findCollide *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->signalFindCollideFinish(); break;
        case 1: _t->signalStartPathCalc((*reinterpret_cast< QList<QGraphicsItem*>(*)>(_a[1]))); break;
        case 2: _t->slotFindCollideInScene((*reinterpret_cast< QList<QGraphicsItem*>(*)>(_a[1]))); break;
        case 3: _t->slotPathIsCalc(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QGraphicsItem*> >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QGraphicsItem*> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (findCollide::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&findCollide::signalFindCollideFinish)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (findCollide::*_t)(QList<QGraphicsItem*> );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&findCollide::signalStartPathCalc)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject findCollide::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_findCollide.data,
      qt_meta_data_findCollide,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *findCollide::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *findCollide::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_findCollide.stringdata0))
        return static_cast<void*>(const_cast< findCollide*>(this));
    return QObject::qt_metacast(_clname);
}

int findCollide::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void findCollide::signalFindCollideFinish()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void findCollide::signalStartPathCalc(QList<QGraphicsItem*> _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
