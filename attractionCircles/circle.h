#ifndef CIRCLE_H
#define CIRCLE_H

#include <QWidget>
#include <QGraphicsItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsSceneMouseEvent>

#define CIRCLE_SIZE         64  // Circle size
#define DEF_CIRCLE_SPEED    1   // Default circle speed
#define ADDMISSION          1   // Amission for circle center point

#define ANIMATION_SPEED     1000 / 33   // In ms between frames

#define DEF_POS_X           400
#define DEF_POS_Y           250

class Circle : public QGraphicsEllipseItem
{
public:
    Circle(QPointF *pointF, qreal opacity);
    ~Circle();

private:
    void moveCircleTo(QPointF point);

private:
    bool moveIsFree = true;     // Collide circle status
    bool mouseMoveNow = false;  // Mouse move state
    bool mousePress = false;
    QPointF targetPoint = QPointF(DEF_POS_X, DEF_POS_Y);

public:
    void releaseMouse(void);
    virtual void advance(int phase);

    // QGraphicsItem interface
protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
};

#endif // CIRCLE_H
