#-------------------------------------------------
#
# Project created by QtCreator 2017-05-24T09:10:13
#
#-------------------------------------------------

QT       += core gui \
            opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

VERSION = 1.0.0.3
QMAKE_TARGET_COMPANY = TestPrj Co
QMAKE_TARGET_PRODUCT = Attraction circles
QMAKE_TARGET_DESCRIPTION = Simple QGraphics project
QMAKE_TARGET_COPYRIGHT = Maykov Denis

TARGET = attractionCircles
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        attractioncircles.cpp \
    circle.cpp \
    graphicsview.cpp \
    findcollide.cpp \
    pathcalc.cpp

HEADERS  += attractioncircles.h \
    circle.h \
    graphicsview.h \
    findcollide.h \
    pathcalc.h

FORMS    += attractioncircles.ui

RC_ICONS = ico/ac.ico
