#ifndef FINDCOLLIDE_H
#define FINDCOLLIDE_H

#include <QObject>
#include <QList>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QThread>

#include "pathcalc.h"

#define THREAD_DELAY 5

class findCollide : public QObject
{
    Q_OBJECT
public:
    explicit findCollide(QObject *parent = 0);
    ~findCollide();

signals:
    void signalFindCollideFinish(void);
    void signalStartPathCalc(QList<QGraphicsItem *>);

public slots:
    void slotFindCollideInScene(QList<QGraphicsItem *>);
    void slotPathIsCalc(void);

private:
    pathCalc *pCalc;
    QGraphicsScene localScene;
};

#endif // FINDCOLLIDE_H
