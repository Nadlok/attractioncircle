#ifndef PATHCALC_H
#define PATHCALC_H

#include <QObject>
#include <QList>
#include <QGraphicsScene>
#include <QGraphicsItem>

#include <thread>
#include <unistd.h>
#include <vector>

struct itemPoint {

    itemPoint (double x, double y) :
        x(x), y(y) {}
    double x;
    double y;
};

class pathCalc : public QObject
{
    Q_OBJECT
public:
    explicit pathCalc(QObject *parent = 0);
    ~pathCalc();

signals:
    void signalPathIsCalc(void);

public slots:
    void slotPatchCalcStart(QList<QGraphicsItem *> allItems);

private:
    bool threadStart = false;
    bool calcEnd = false;

    std::vector<itemPoint> newPoint;

private:
    static void calcNewItemPoint(std::vector<itemPoint> &point, bool &endCalc);
    static void applyNewPath(QList<QGraphicsItem *> allItems, std::vector<itemPoint> &point, bool &endCalc, bool &threadStatus);
};

#endif // PATHCALC_H
