#ifndef MYGRAPHICSVIEW_H
#define MYGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QMouseEvent>
#include <QTimer>
#include <QThread>
#include "findcollide.h"

#define SCENE_W 800
#define SCENE_H 550

class GraphicsView : public QGraphicsView
{
    Q_OBJECT

public:
    explicit GraphicsView(QWidget *parent = 0);
    ~GraphicsView();

signals:
    void signalSendItemList(QList<QGraphicsItem *>);
    void signalSendNumItems(int numItems);

public slots:    
    void slotUpdateSceneSize(QResizeEvent *event);
    void slotUpdateItemsOpacity(int opacity);
    void slotDeleteAllCircles(void);
    void slotUpdateScene(void);
    void slotCollideIsFinished(void);

private:
    bool deleAllItem = false;
    bool collideIsFinished = true;
    bool itemsPathUpdate = true;
    qreal itemOpacity;
    QGraphicsScene *gScene;
    QTimer *timMoveCircle;
    findCollide *fCollide;
    QThread *collideThread;
    bool deleteItem = false;
    QGraphicsItem *itemForDelete;
    bool addNewItem = false;
    QPointF newItemP;

    // QWidget interface
protected:
    void mouseReleaseEvent(QMouseEvent *event);
};

#endif // MYGRAPHICSVIEW_H
