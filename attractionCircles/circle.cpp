#include "circle.h"
#include "math.h"

/*
 * Create circle item on QGraphicsScene
*/
Circle::Circle(QPointF *pointF, qreal opacity) {

    QColor circleColor = QColor(rand() % 255, rand() % 255, rand() % 255);

    setBrush(circleColor);

    setRect(0, 0, CIRCLE_SIZE, CIRCLE_SIZE);

    setPos(pointF->x(), pointF->y());

    setFlags(QGraphicsItem::ItemIsMovable);

    setOpacity(opacity);
}

Circle::~Circle() {


}

/*
 * Compute and move cirle to next position
*/
void Circle::moveCircleTo(QPointF point) {

    qreal srcX = pos().x();
    qreal srcY = pos().y();

    qreal dstX = point.x();
    qreal dstY = point.y();

    if(srcX < (dstX - ADDMISSION) || srcX > (dstX + ADDMISSION) ||
       srcY < (dstY - ADDMISSION) || srcY > (dstY + ADDMISSION)) {

        qreal nextX = srcX;
        qreal nextY = 0;

        if((dstX - ADDMISSION) > srcX) {

            nextX = srcX + DEF_CIRCLE_SPEED;
        }
        else if((dstX + ADDMISSION) < srcX) {

            nextX = srcX - DEF_CIRCLE_SPEED;
        }

        qreal dX = nextX - srcX;

        if(dX != 0) {

            nextY = dX * (dstY - srcY) / (dstX - srcX) ;

            if(abs(nextY) > DEF_CIRCLE_SPEED) {

                dX = dX * (DEF_CIRCLE_SPEED / abs(nextY));

                nextY = srcY + DEF_CIRCLE_SPEED * nextY / abs(nextY);
            }
            else {

                nextY += srcY;
            }
        }
        else if((dstY - ADDMISSION) > srcY) {

            nextY = srcY + DEF_CIRCLE_SPEED;
        }
        else if((dstY + ADDMISSION) < srcY) {

            nextY = srcY - DEF_CIRCLE_SPEED;
        }

        qreal dY = nextY - srcY;

        moveBy(dX, dY);
    }
}

void Circle::releaseMouse() {    

    if(mousePress) {

        mousePress = false;

        this->setCursor(QCursor(Qt::ArrowCursor));


    }
}

/*
 * Animation of the motion of a circle
*/
void Circle::advance(int phase) {

    switch (phase) {

    case 0: {

        if(data(0).toBool()) {

            setData(0, false);

            moveIsFree = true;
        }

        if(moveIsFree) {

            if(collidingItems().size() != 0) {

                moveIsFree = false;
            }
            else {

                moveIsFree = true;
            }
        }

        if(data(1).toPointF() != QPointF()) {

            targetPoint = data(1).toPointF();

            setData(1, 0);
        }

        if(data(2).toBool()) {

            setData(2, false);

            /*
             * Delete choose item
            */
            if(!mouseMoveNow) {

                delete this;
            }

            mouseMoveNow = false;
        }
    } break;

    case 1: {

        if(moveIsFree && !mouseMoveNow) {

            moveCircleTo(targetPoint);
        }
    } break;

    default: {

    } break;
    }
}

void Circle::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {

    if(mousePress) {

        mouseMoveNow = true;

        this->setCursor(QCursor(Qt::ClosedHandCursor));

        this->setPos(mapToScene(event->pos().x() - CIRCLE_SIZE / 2,
                                event->pos().y() - CIRCLE_SIZE / 2));        
    }
}

void Circle::mousePressEvent(QGraphicsSceneMouseEvent *event) {

    if(event->button() == Qt::LeftButton) {

        mousePress = true;
    }

}

void Circle::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {

    if(event->button() == Qt::LeftButton) {

        this->setCursor(QCursor(Qt::ArrowCursor));

        mousePress = false;
    }
}
