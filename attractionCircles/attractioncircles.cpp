#include "attractioncircles.h"
#include "ui_attractioncircles.h"
#include "circle.h"
#include "graphicsview.h"
#include <QIcon>

attractionCircles::attractionCircles(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::attractionCircles)
{
    ui->setupUi(this);

    setWindowTitle(QString("Attraction circles %1").arg(VERSION));

    /*
     * QGraphicsView configuration
    */
    graphicsView = new GraphicsView(ui->graphicsView);
    graphicsView->setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers)));
    graphicsView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    graphicsView->setRenderHint(QPainter::Antialiasing, true);
    //graphicsView->setCacheMode(QGraphicsView::CacheBackground);
    graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    graphicsView->update();

    /*
     * Connect opacity slider signal and update depending values
    */
    connect(ui->slOpacity, SIGNAL(valueChanged(int)),
            graphicsView, SLOT(slotUpdateItemsOpacity(int)));

    ui->label->setNum(ui->slOpacity->value());
    graphicsView->slotUpdateItemsOpacity(ui->slOpacity->value());

    /*
     * Connect button clicked signal for delete all circles
    */
    connect(ui->btnDeleteAll, SIGNAL(clicked(bool)),
            graphicsView, SLOT(slotDeleteAllCircles()));

    /*
     * Connect signal for update number of circles field
    */
    connect(graphicsView, SIGNAL(signalSendNumItems(int)),
            ui->lblNumCircles, SLOT(setNum(int)));
}

attractionCircles::~attractionCircles()
{
    delete ui;
}

void attractionCircles::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void attractionCircles::resizeEvent(QResizeEvent *event)
{
    graphicsView->slotUpdateSceneSize(event);
}
