#include "pathcalc.h"
#include "graphicsview.h"

pathCalc::pathCalc(QObject *parent) : QObject(parent) {


}

pathCalc::~pathCalc() {


}

void pathCalc::slotPatchCalcStart(QList<QGraphicsItem *> allItems) {

    if(!threadStart) {

        int numItems = allItems.size();

        newPoint.erase(std::begin(newPoint), std::end(newPoint));

        if(numItems > 0) {

            foreach (QGraphicsItem *circle, allItems) {

                newPoint.push_back(itemPoint(circle->pos().x(),
                                             circle->pos().y()));
            }

            threadStart = true;
            calcEnd = false;

            std::thread threadCalc(pathCalc::calcNewItemPoint,
                                   std::ref(newPoint),
                                   std::ref(calcEnd));

            std::thread threadApplyPath(pathCalc::applyNewPath,
                                        std::ref(allItems),
                                        std::ref(newPoint),
                                        std::ref(calcEnd),
                                        std::ref(threadStart));

            threadApplyPath.detach();
            threadCalc.join();

            emit signalPathIsCalc();
        }
        else {

            emit signalPathIsCalc();
        }
    }
}

void pathCalc::applyNewPath(QList<QGraphicsItem *> allItems, std::vector<itemPoint> &point, bool &endCalc, bool &threadStatus) {

    while(!endCalc) {

    }

    int count = 0;

    foreach (QGraphicsItem *circle, allItems) {

        itemPoint newPos = point.at(count);
        circle->setData(1, QPointF(newPos.x, newPos.y));

        count++;
    }

    threadStatus = false;
}

void pathCalc::calcNewItemPoint(std::vector<itemPoint> &point, bool &endCalc) {

    int numItems = point.size();

    std::vector<itemPoint> pVect = point;
    /*
     * Calc new position
    */
    for(int i = 0; i < numItems; i++) {

        itemPoint baseItem = pVect.at(i);

        double maxForceVal = 0.0;
        int maxForceCount = 0;

        /*
         * Correct item position considering the force
        */
        for(int j = 0; j < numItems; j++) {

            itemPoint targetItem = pVect.at(j);

            double dX = baseItem.x - targetItem.x;
            double dY = baseItem.y - targetItem.y;

            double lPath = sqrt(dX * dX + dY * dY);

            double force = 0.0;

            if(lPath != 0) {

                force = 1 / lPath - 1 / (lPath * lPath);
            }

            if(j == 0) {

                maxForceVal = std::abs(force);

                maxForceCount = j;
            }
            else if(std::abs(force) > maxForceVal) {

                maxForceVal = std::abs(force);

                maxForceCount = j;
            }
        }

        /*
         * Push result
        */
        itemPoint maxPoint = pVect.at(maxForceCount);
        point.at(i).x = maxPoint.x;
        point.at(i).y = maxPoint.y;
    }

    endCalc = true;
}
