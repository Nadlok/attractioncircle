#include "findcollide.h"

findCollide::findCollide(QObject *parent) : QObject(parent) {

    /*
     * Configuration class for calculation new position
    */
    pCalc = new pathCalc();
    connect(this, SIGNAL(signalStartPathCalc(QList<QGraphicsItem *>)),
            pCalc, SLOT(slotPatchCalcStart(QList<QGraphicsItem *>)));
    connect(pCalc, SIGNAL(signalPathIsCalc()),
            this, SLOT(slotPathIsCalc()));
}

findCollide::~findCollide() {

}

void findCollide::slotFindCollideInScene(QList<QGraphicsItem *> allItems) {

    foreach (QGraphicsItem *circle, allItems) {

        try {

            circle->setData(0, true);
        }
        catch (...) {


        }

        thread()->msleep(THREAD_DELAY);
    }

    emit signalStartPathCalc(allItems);
}

void findCollide::slotPathIsCalc() {

    emit signalFindCollideFinish();
}

