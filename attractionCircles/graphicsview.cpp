#include "graphicsview.h"
#include "circle.h"
#include <thread>

GraphicsView::GraphicsView(QWidget *parent) : QGraphicsView(parent) {

    /*
     * Graphics scene configuration
    */
    gScene = new QGraphicsScene(0,0, SCENE_W, SCENE_H, this);
    this->setScene(gScene);

    /*
     * Configuration find collides object
    */
    qRegisterMetaType<QList<QGraphicsItem *>>("QList<QGraphicsItem *>");

    fCollide = new findCollide();
    collideThread = new QThread(this);

    connect(fCollide, SIGNAL(destroyed(QObject*)),
            collideThread, SLOT(quit()));

    connect(this, SIGNAL(signalSendItemList(QList<QGraphicsItem *>)),
            fCollide, SLOT(slotFindCollideInScene(QList<QGraphicsItem *>)));

    connect(fCollide, SIGNAL(signalFindCollideFinish()),
            this, SLOT(slotCollideIsFinished()));

    connect(collideThread, SIGNAL(finished()),
            collideThread, SLOT(deleteLater()));

    fCollide->moveToThread(collideThread);
    collideThread->start();

    /*
     * Animation timer configuration
    */
    timMoveCircle = new QTimer(this);
    connect(timMoveCircle, SIGNAL(timeout()),
            gScene, SLOT(advance()));
    connect(timMoveCircle, SIGNAL(timeout()),
            this, SLOT(slotUpdateScene()));
    timMoveCircle->start(ANIMATION_SPEED);
}

GraphicsView::~GraphicsView() {

    delete fCollide;

    collideThread->wait();
}

void GraphicsView::mouseReleaseEvent(QMouseEvent *event) {

    newItemP = mapToScene(event->pos().x() - CIRCLE_SIZE / 2,
                          event->pos().y() - CIRCLE_SIZE / 2);

    QGraphicsItem *item = gScene->itemAt(event->pos(), QTransform());

    if(event->button() == Qt::RightButton) {

        // If free space
        if(item == NULL) {

            if(!addNewItem) {

                addNewItem = true;
            }
        }
    }
    else if(event->button() == Qt::LeftButton) {

        if(!deleteItem) {

            deleteItem = true;

            itemForDelete = item;
        }
    }

    QGraphicsView::mouseReleaseEvent(event);
}

void GraphicsView::slotUpdateSceneSize(QResizeEvent *event) {

    //gScene->setSceneRect(QRectF(QPointF(0, 0), event->size()));

    QGraphicsView::resizeEvent(event);
}

void GraphicsView::slotUpdateItemsOpacity(int opacity) {

    QList<QGraphicsItem *> allCircle = gScene->items(gScene->sceneRect(),
                                                     Qt::IntersectsItemShape,
                                                     Qt::DescendingOrder,
                                                     QTransform());

    itemOpacity = qreal(opacity) / 100;

    foreach (QGraphicsItem *circle, allCircle) {

        circle->setOpacity(itemOpacity);
    }
}

void GraphicsView::slotDeleteAllCircles() {

    deleAllItem = true;
}

void GraphicsView::slotUpdateScene() {

    if(addNewItem) {

        addNewItem = false;

        gScene->addItem(new Circle(&newItemP, itemOpacity));
    }

    if(deleteItem && collideIsFinished) {

        deleteItem = false;

        itemForDelete->setData(2, true);
    }
    else if(deleAllItem && collideIsFinished) {

        deleAllItem = false;

        QList<QGraphicsItem *> allCircle = gScene->items(gScene->sceneRect(),
                                                         Qt::IntersectsItemShape,
                                                         Qt::AscendingOrder,
                                                         QTransform());

        foreach (QGraphicsItem *circle, allCircle) {

            delete circle;
        }
    }
    else if(collideIsFinished) {

        collideIsFinished = false;

        QList<QGraphicsItem *> allCircle = gScene->items(gScene->sceneRect(),
                                                         Qt::IntersectsItemShape,
                                                         Qt::AscendingOrder,
                                                         QTransform());

        emit signalSendNumItems(allCircle.size());

        emit signalSendItemList(allCircle);
    }
}

void GraphicsView::slotCollideIsFinished() {

    collideIsFinished = true;
}
