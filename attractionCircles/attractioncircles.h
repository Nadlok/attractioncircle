#ifndef ATTRACTIONCIRCLES_H
#define ATTRACTIONCIRCLES_H

#include <QWidget>
#include <QGLWidget>
#include <QResizeEvent>
#include "graphicsview.h"

#define VERSION "1.0.0.3"

namespace Ui {
class attractionCircles;
}

class attractionCircles : public QWidget
{
    Q_OBJECT

public:
    explicit attractionCircles(QWidget *parent = 0);
    ~attractionCircles();

protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *event);

private:
    Ui::attractionCircles *ui;
    GraphicsView *graphicsView;
};

#endif // ATTRACTIONCIRCLES_H
